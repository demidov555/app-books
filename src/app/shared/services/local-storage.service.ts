import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  public getItem<T>(name: string): T[] {
    const item = localStorage.getItem(name);

    return item ? [...JSON.parse(item)] : null;
  }

  public saveItem(name: string, value: any): void {
    localStorage.setItem(name, JSON.stringify(value));
  }

  public getItemAtString(name: string): string {
    return localStorage.getItem(name);
  }

  public saveItemAtString(name: string, value: any): void {
    localStorage.setItem(name, value);
  }

  public destroyItem(item: string): void {
    localStorage.removeItem(item);
  }

}