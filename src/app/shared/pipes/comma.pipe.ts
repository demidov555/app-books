import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
  name: 'commaPipe'
})
export class CommaPipe implements PipeTransform {
  transform(item: string | number, isLast: boolean): string {
    return `${item}${isLast ? '' : ', '}`;
  }
}