/**
 * Get random int
 */
const getRandomInt = (min: number, max: number): number => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Get a unique id
 */
export const ID = (): number => {
  const lenghtId = 3;
  let timestamp = +new Date;
  let parts = timestamp.toString().split('').reverse();
  let id = '';
  
  for(let i = 0; i < lenghtId; i++) {
    const index = getRandomInt(0, parts.length - 1);
    id += parts[index];	 
  }
  
  return Number(id);
}