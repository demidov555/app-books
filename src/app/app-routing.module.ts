import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./routed/books-home-page/books-home-page.module').then(module => module.BooksHomePageModule)
  },
  {
    path: 'create',
    loadChildren: () => import('./routed/books-create-page/books-create-page.module').then(module => module.BooksCreatePageModule)
  },
  {
    path: 'edit/:id',
    loadChildren: () => import('./routed/books-edit-page/books-edit-page.module').then(module => module.BooksEditPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}