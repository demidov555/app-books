import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { LocalStorageService } from '../../../shared/services/local-storage.service';
import { IBook } from '@app/modules/books/models/books.models';
import { ID } from '@app/shared/utils/uuid';

@Injectable()
export class BooksService {

  constructor(
    private localStorageService: LocalStorageService
  ) {}

  public loadBooks(): Observable<IBook[]> {
    const books = this.getAllBooks();

    return of(books);
  }

  public getBookById(id: number): Observable<IBook> {
    const books = this.getAllBooks();
    const findBook = books.filter((book: IBook) => book.id === id);
  
    return of(findBook[0]);
  }

  public getSelected(): Observable<string> {
    const selected = this.localStorageService.getItemAtString('selected');
  
    return of(selected);
  }

  public createBook(book: IBook): Observable<IBook> {
    const books = this.getAllBooks() || [];

    book.id = ID();
    books.push(book);
    this.localStorageService.saveItem('books', books);

    return of(book);
  }

  public updateBook(book: IBook): Observable<IBook> {
    const books = this.getAllBooks();

    books.forEach((item, index) => {
      if (item['id'] === book.id) {
        books[index] = book;
        return;
      }
    });

    this.localStorageService.saveItem('books', books);

    return of(book);
  }

  public removeBook(id: number): Observable<number> {
    const books = this.getAllBooks();

    if (Array.isArray(books)) {
      const newListBooks = books.filter((book: IBook) => book.id !== id);
      this.localStorageService.saveItem('books', newListBooks);
    }

    return of(id);
  }

  public sortBooks(selectedItem: string): Observable<IBook[]> {
    const books = this.getAllBooks();
  
    if (selectedItem === 'DATE_PUBLICATION') {
      books.sort((a: IBook, b: IBook) => b.datePublication > a.datePublication ? 1 : -1);
    } else {
      books.sort((a: IBook, b: IBook) => a.title > b.title ? 1 : -1);
    }

    this.localStorageService.saveItem('books', books);
    this.localStorageService.saveItemAtString('selected', selectedItem);

    return of(books);
  }

  private getAllBooks(): IBook[] {
    return this.localStorageService.getItem<IBook>('books');
  }
}