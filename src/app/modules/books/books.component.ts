import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BooksService } from './services/books.service';
import { IBook } from './models/books.models';
import { MatSelectChange } from '@angular/material';
import { SELECT_SORT_BOOKS } from './models/books.consts';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html'
})
export class BooksComponent implements OnInit {

  public books$: Observable<IBook[]>;
  public selectSortBooks = SELECT_SORT_BOOKS;
  public selected: string;

  constructor(
    private booksService: BooksService,
    private router: Router
  ) {}

  public ngOnInit(): void {
    this.loadBooks();

    this.booksService.getSelected()
      .subscribe((selected: string) => this.selected = selected);
  }

  public loadBooks(): void {
    this.books$ = this.booksService.loadBooks();
  }

  public navigateToCreateBookPage(): void {
    this.router.navigate(['create']);
  }

  public editBook(id: number): void {
    this.router.navigate([`edit/${id}`]);
  }

  public removeBook(id: number): void {
    this.booksService.removeBook(id)
      .subscribe(() => this.loadBooks());
  }

  public sortBooks(selectedItem: MatSelectChange): void {
    this.books$ = this.booksService.sortBooks(selectedItem.value);
  }
}