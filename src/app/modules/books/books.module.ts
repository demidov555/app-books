import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { BooksComponent } from './books.component';
import { ListBooksComponent } from './components/list-books/list-books.component';
import { CreateBookComponent } from './components/create-book/create-book.component';
import { EditBookComponent } from './components/edit-book/edit-book.component';
import { BooksService } from './services/books.service';
import { AuthorDialogComponent } from './components/author-dialog/author-dialog.component';
import { BookFormComponent } from './components/book-form/book-form.component';
import { ItemBookComponent } from './components/item-book/item-book.component';

@NgModule({
  imports: [
    FormsModule,
    SharedModule
  ],
  exports: [BooksComponent, CreateBookComponent, EditBookComponent],
  declarations: [
    BooksComponent,
    ListBooksComponent,
    CreateBookComponent,
    EditBookComponent,
    AuthorDialogComponent,
    BookFormComponent,
    ItemBookComponent
  ],
  entryComponents: [AuthorDialogComponent]
})
export class BooksModule {
  static forRoot(): ModuleWithProviders<BooksModule> {
    return {
      ngModule: BooksModule,
      providers: [BooksService]
    };
  }
}