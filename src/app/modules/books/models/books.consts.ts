export const SELECT_SORT_BOOKS = [
  {
    id: 0,
    title: 'Date publication',
    codeName: 'DATE_PUBLICATION'
  },
  {
    id: 1,
    title: 'Title',
    codeName: 'TITLE'
  }
];