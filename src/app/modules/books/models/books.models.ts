export interface IAuthor {
  id?: number;
  firstname: string;
  lastname: string;
}

export interface IBook {
  id: number;
  title: string;
  authors: IAuthor[];
  datePublication: string;
  ISBN: string;
}