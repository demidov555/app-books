import { Component, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { IBook } from '@app/modules/books/models/books.models';

@Component({
  selector: 'app-list-books',
  templateUrl: './list-books.component.html'
})
export class ListBooksComponent implements OnChanges {

  @Input() books: IBook[];
  @Output() editBookEvent = new EventEmitter<number>();
  @Output() removeBookEvent = new EventEmitter<number>();

  public get isBooks(): boolean {
    return Array.isArray(this.books) && Boolean(this.books.length);
  }

  public ngOnChanges(change: SimpleChanges): void {
    if (change.books.currentValue) {
      this.books = [...change.books.currentValue];
    }
  }

  public editBook(id: number): void {
    this.editBookEvent.emit(id);
  }

  public removeBook(id: number): void {
    this.removeBookEvent.emit(id);
  }
}