import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { BooksService } from '@app/modules/books/services/books.service';

const LABEL_ACTION_BUTTON = 'Create a book';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html'
})
export class CreateBookComponent {

  public labelActionButton = LABEL_ACTION_BUTTON;
  public formControl = this.fb.control({
    title: null,
    authors: [],
    datePublication: null,
    ISBN: null
  });

  constructor(
    private router: Router,
    private booksService: BooksService,
    public fb: FormBuilder
  ) {}

  public createNewBook(): void {
    this.booksService.createBook(this.formControl.value)
      .subscribe(() => this.navigateToBooksPage());
  }

  public navigateToBooksPage(): void {
    this.router.navigate(['/home']);
  }
}