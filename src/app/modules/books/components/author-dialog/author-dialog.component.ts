import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { IAuthor } from '@app/modules/books/models/books.models';

const MAX_LENGTH_FIELDS = 30;

@Component({
  selector: 'app-author-dialog',
  templateUrl: './author-dialog.component.html'
})
export class AuthorDialogComponent implements OnInit {

  public form = this.fb.group({
    id: [null],
    firstname: [null, [Validators.required, Validators.maxLength(MAX_LENGTH_FIELDS)]],
    lastname: [null, [Validators.required, Validators.maxLength(MAX_LENGTH_FIELDS)]]
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) private author: IAuthor,
    private dialogRef: MatDialogRef<AuthorDialogComponent>,
    private fb: FormBuilder
  ) {}

  public ngOnInit(): void {
    if (this.author) {
      this.form.setValue(this.author);
    }
  }

  public cancel(): void {
    this.dialogRef.close();
  }

}
