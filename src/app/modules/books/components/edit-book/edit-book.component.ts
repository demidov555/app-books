import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { BooksService } from '@app/modules/books/services/books.service';
import { IBook } from '../../models/books.models';

const LABEL_ACTION_BUTTON = 'Update a book';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html'
})
export class EditBookComponent implements OnInit {

  @Input() book: IBook;

  public labelActionButton = LABEL_ACTION_BUTTON;
  public formControl = this.fb.control({
    title: null,
    authors: [],
    datePublication: null,
    ISBN: null
  });

  constructor(
    private router: Router,
    private booksService: BooksService,
    public fb: FormBuilder
  ) {}

  public ngOnInit(): void {
    if (!this.book) {
      return;
    }

    this.formControl.patchValue(this.book);
  }

  public updateBook(): void {
    this.booksService.updateBook(this.formControl.value)
      .subscribe(() => this.navigateToBooksPage());
  }

  public navigateToBooksPage(): void {
    this.router.navigate(['/home']);
  }
}