import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, forwardRef, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { AuthorDialogComponent } from '@app/modules/books/components/author-dialog/author-dialog.component';
import { IAuthor, IBook } from '@app/modules/books/models/books.models';
import { ID } from '@app/shared/utils/uuid';

const MAX_LENGTH_TITLE = 30;
const MIN_LENGTH_AUTHOR = 1;
const REG_EXP_ISBN = /^(?=(?:\D*\d){10}(?:(?:\D*\d){3})?$)[\d-]+$/;

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: [ './book-form.component.scss' ],
    providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => BookFormComponent),
    multi: true
  }]
})
export class BookFormComponent implements OnInit, ControlValueAccessor {

  @Input() labelActionButton: string;
  @Output() createNewBookEvent = new EventEmitter<IBook>();
  @Output() cancelEvent = new EventEmitter<void>();

  public minDate = new Date(1800, 0, 1);
  public maxDate = new Date(2020, 0, 1);
  public form = this.fb.group({
    id: [null],
    title: [null, [Validators.required, Validators.maxLength(MAX_LENGTH_TITLE)]],
    authors: [[], [Validators.required, Validators.minLength(MIN_LENGTH_AUTHOR)]],
    datePublication: [null, [Validators.required]],
    ISBN: [null, Validators.pattern(REG_EXP_ISBN)]
  });

  private onChange = (value: any) => {};
  private destroy$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog
  ) {}

  public ngOnInit(): void {
    this.form.valueChanges
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(value => this.onChange(value))
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => {}): void {
  }

  public writeValue(book: IBook): void {
    if (book) {
      this.form.patchValue(book);
    }
  }

  public createNewBook(): void {
    this.createNewBookEvent.emit(this.form.value);
  }

  public removeChipItem(item): void {
    let array: IAuthor[] = [...this.form.get('authors').value];
    const index = array.indexOf(item);

    if (index >= 0) {
      array.splice(index, 1);
    }

    this.form.get('authors').setValue(array);
  }

  public openDialog(author?: IAuthor): void {
    const authorData = author ? author : null;
    const dialogRef = this.dialog.open(AuthorDialogComponent, {
      width: '450px',
      data: authorData
    });

    dialogRef.afterClosed()
      .subscribe((author: IAuthor) => {
        if (author) {
          const newAuhorsArray = this.getNewAuthorsArray(author);

          this.form.get('authors').setValue(newAuhorsArray);
        }
      });
  }

  private getNewAuthorsArray(author: IAuthor): IAuthor[] {
    let newAuthorsArray: IAuthor[] = [...this.form.get('authors').value];
    const updateAuthor = newAuthorsArray.find((item, index) => {
      if (item.id === author.id) {
        newAuthorsArray[index] = author;
        return true;
      }
    });

    if (!updateAuthor) {
      author.id = ID();
      newAuthorsArray.push(author);
    }

    return newAuthorsArray;
  }

}