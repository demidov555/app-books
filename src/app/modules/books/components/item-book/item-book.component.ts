import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IBook } from '../../models/books.models';

@Component({
  selector: 'app-item-book',
  templateUrl: './item-book.component.html'
})
export class ItemBookComponent {
  @Input() book: IBook;

  @Output() editBook = new EventEmitter<number>();
  @Output() removeBook = new EventEmitter<number>();
}
