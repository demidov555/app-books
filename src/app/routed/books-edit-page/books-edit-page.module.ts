import { NgModule } from '@angular/core';
import { BooksModule } from '@app/modules/books/books.module';
import { SharedModule } from '@app/shared/shared.module';
import { BooksEditPageRoutingModule } from './books-edit-page-routing.module';
import { BooksEditPageComponent } from './books-edit-page.component';
import { BooksEditPageResolver } from './books-edit-page.resolver';

@NgModule({
  imports: [
    SharedModule,
    BooksModule.forRoot(),
    BooksEditPageRoutingModule
  ],
  providers: [BooksEditPageResolver],
  declarations: [BooksEditPageComponent]
})
export class BooksEditPageModule {}
