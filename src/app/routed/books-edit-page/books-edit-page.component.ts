import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IBook } from '@app/modules/books/models/books.models';

@Component({
  selector: 'app-books-edit-page',
  templateUrl: './books-edit-page.component.html'
})
export class BooksEditPageComponent {

  public book: IBook = this.activatedRoute.snapshot.data.book;

  constructor(
    private activatedRoute: ActivatedRoute
  ) {}

}