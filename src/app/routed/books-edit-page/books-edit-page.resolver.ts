import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { IBook } from '@app/modules/books/models/books.models';
import { BooksService } from '@app/modules/books/services/books.service';
import { Observable } from 'rxjs';

@Injectable()
export class BooksEditPageResolver implements Resolve<any> {

  constructor(private booksService: BooksService) {}

  public resolve(activatedRoute: ActivatedRouteSnapshot): Observable<IBook> {
    const bookId =  Number(activatedRoute.paramMap.get('id'));

    if (!bookId) {
      return;
    }

    return this.booksService.getBookById(bookId);
  }
}