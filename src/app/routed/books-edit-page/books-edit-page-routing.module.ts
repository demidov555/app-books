import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksEditPageComponent } from './books-edit-page.component';
import { BooksEditPageResolver } from './books-edit-page.resolver';

const routes: Routes = [
  {
    path: '',
    component: BooksEditPageComponent,
    resolve: { book: BooksEditPageResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BooksEditPageRoutingModule {}