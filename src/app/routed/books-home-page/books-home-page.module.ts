import { NgModule } from '@angular/core';
import { BooksModule } from '@app/modules/books/books.module';
import { BooksHomePageRoutingModule } from './books-home-page-routing.module';
import { BooksHomePageComponent } from './books-home-page.component';

@NgModule({
  imports: [
    BooksModule.forRoot(),
    BooksHomePageRoutingModule
  ],
  declarations: [BooksHomePageComponent]
})
export class BooksHomePageModule {}
