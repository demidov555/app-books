import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksHomePageComponent } from './books-home-page.component';

const routes: Routes = [
  {
    path: '',
    component: BooksHomePageComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BooksHomePageRoutingModule {}