import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksCreatePageComponent } from './books-create-page.component';

const routes: Routes = [
  {
    path: '',
    component: BooksCreatePageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BooksCreatePageRoutingModule {}