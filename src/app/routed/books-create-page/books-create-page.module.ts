import { NgModule } from '@angular/core';
import { BooksModule } from '@app/modules/books/books.module';
import { SharedModule } from '@app/shared/shared.module';
import { BooksCreatePageRoutingModule } from './books-create-page-routing.module';
import { BooksCreatePageComponent } from './books-create-page.component';

@NgModule({
  imports: [
    SharedModule,
    BooksModule.forRoot(),
    BooksCreatePageRoutingModule
  ],
  declarations: [BooksCreatePageComponent]
})
export class BooksCreatePageModule {}
