# MyBooksApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.6.

## Run app

- `npm i`
- `ng serve`
- Navigate to `http://localhost:4200/`

## Arch


- shared - повторяющиеся элементы. Предназначин для расшаривания в другие модули.
- routed - routing и routed модули. Структура соответствует UI страницам приложения.
- modules - domain модули

## Backend
 - все запросы эмулируются при помощи localstorage